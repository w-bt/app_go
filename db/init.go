package db

import (
	"database/sql"
	"log"

	conf "bitbucket.org/w-bt/sharing/app_go/config/environments"
	_ "github.com/lib/pq"
)

var (
	db  *sql.DB
	err error
)

const (
	DRIVER_POSTGRES = "postgres"
)

func Init() {
	if conf.ENV == nil {
		log.Fatalln("Postgre Init Failed! Exiting...")
	}
	return
}

func Get() *sql.DB {
	if db != nil {
		err = db.Ping()
		if err == nil {
			return db
		}
		log.Println("[db] db connection: Master disconnected. Creating new connection..", err)
	}

	db, err = sql.Open(DRIVER_POSTGRES, conf.ENV.PostgreSQL.Master)
	if err != nil {
		log.Println("[db] problem establishing connection sqlx.Connect:", err)
		return nil
	}

	db.SetMaxIdleConns(conf.ENV.PostgreSQL.MaxIdleConn)
	db.SetMaxOpenConns(conf.ENV.PostgreSQL.MaxOpenConn)

	return db
}
