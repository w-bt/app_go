package redis

import (
	"fmt"

	redigo "github.com/garyburd/redigo/redis"
)

// Rename renames key to newkey
func Rename(oldKey, newKey string) (string, error) {
	conn := getConnection()
	if conn != nil {

		defer conn.Close()
		return redigo.String(conn.Do("RENAME", oldKey, newKey))
	}
	return "", fmt.Errorf("Failed to obtain connection newKey %s oldKey%s", newKey, oldKey)
}

// Exists returns if key exists
// return 1 if the key exists.
// return 0 if the key does not exist.
func Exists(key string) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("EXISTS", key))
	}
	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)

}

// Expire set a timeout on key
// return 1 if the timeout was set
// return 0 if key does not exist
func Expire(key string, expiryInSeconds int) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("EXPIRE", key, expiryInSeconds))
	}

	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)

}

// TTL returns the expire of a key on seconds
// return -1 if the key doesn't have an expiry
// return -2 if the key isn't exist
func TTL(key string) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("TTL", key))
	}

	return int(0), fmt.Errorf("Failed to obtain connection [key: %s]", key)
}
