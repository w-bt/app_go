package redis

import (
	"fmt"

	redigo "github.com/garyburd/redigo/redis"
)

// HDel specified field from the hash stored at key
func HDel(key string, fields ...string) (int, error) {
	conn := getConnection()
	if conn != nil {

		defer conn.Close()
		return redigo.Int(conn.Do("HDEL", redigo.Args{}.Add(key).AddFlat(fields)...))

	}
	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)
}

// HExist return 1 if the field exist in hash stored at key otherwise return 0
func HExist(key, field string) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("HEXIST", key, field))
	}
	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)

}

// HGet return value associated with field in the hash stored at key
func HGet(key, field string) (string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		result, err := redigo.String(conn.Do("HGET", key, field))
		if err != nil && err == redigo.ErrNil {
			err = nil
		}
		return result, err
	}

	return "", fmt.Errorf("Failed to obtain connection key %s", key)

}

// HMGet return values associated with the fields in the hash stored at key
// for every field that does not exist in the hash, a nil value is returned
// running HMGet on non existing key will return a list of nil values
func HMGet(key string, fields ...string) ([]string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Strings(conn.Do("HMGET", redigo.Args{}.Add(key).AddFlat(fields)...))
	}
	return []string{}, fmt.Errorf("Failed to obtain connection key %s", key)

}

// HMSet sets the specified fields to their respective values in the hash stored at key
func HMSet(key string, hashvalue interface{}) (string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.String(conn.Do("HMSET", redigo.Args{}.Add(key).AddFlat(hashvalue)...))
	}
	return "", fmt.Errorf("Failed to obtain connection key %s", key)

}

// HKeys return all field names in hash stored at key
func HKeys(key string, hashvalue interface{}) ([]string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Strings(conn.Do("HKEYS", redigo.Args{}.Add(key).AddFlat(hashvalue)...))
	}

	return []string{}, fmt.Errorf("Failed to obtain connection key %s", key)

}

func Del(key string) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("DEL", key))
	}

	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)

}

func MultiDel(dbname string, key []string) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("DEL", redigo.Args{}.AddFlat(key)...))
	}

	return int(0), fmt.Errorf("Failed to obtain connection key %v", key)
}

func HGetAll(key string) (map[string]string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		result, err := redigo.StringMap(conn.Do("HGETALL", key))
		if err != nil && err == redigo.ErrNil {
			err = nil
		}
		return result, err
	}
	return nil, fmt.Errorf("Failed to obtain connection key %s", key)

}

// HSet sets the specified fields to their respective values in the hash stored at key
func HSet(key string, hashvalue interface{}) (int64, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int64(conn.Do("HSET", redigo.Args{}.Add(key).AddFlat(hashvalue)...))
	}
	return 0, fmt.Errorf("Failed to obtain connection key %s", key)

}

// HFieldSet set single field their respective values in the hash stored at key
func HFieldSet(key string, field string, value interface{}) (int64, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int64(conn.Do("HSET", key, field, value))
	}
	return 0, fmt.Errorf("Failed to obtain connection key %s", key)
}

func HgetallMultiple(dbname string, keys []string) (res map[string]map[string]string, err error) {
	res = make(map[string]map[string]string)
	conn := getConnection()
	if conn == nil {
		return res, fmt.Errorf("Failed to obtain connection key %+v", keys)
	}
	defer conn.Close()
	for _, key := range keys {
		conn.Send("HGETALL", key)
	}
	conn.Flush()
	for _, key := range keys {
		resultKey, err := redigo.StringMap(conn.Receive())
		if err != nil {
			continue
		}
		res[key] = resultKey
	}
	return
}
func HMGetMultiple(dbname string, keys []string, fields ...string) (res map[string][]string, err error) {
	res = make(map[string][]string)
	conn := getConnection()
	if conn == nil {
		return res, fmt.Errorf("Failed to obtain connection key %+v", keys)
	}
	defer conn.Close()
	for _, key := range keys {
		conn.Send("HMGET", redigo.Args{}.Add(key).AddFlat(fields)...)
	}
	conn.Flush()
	for _, key := range keys {
		resultKey, err := redigo.Strings(conn.Receive())
		if err != nil {
			continue
		}
		res[key] = resultKey
	}
	return
}

func GetMultiple(dbname string, keys []string) (res map[string]string, err error) {
	res = make(map[string]string)
	conn := getConnection()
	if conn == nil {
		return res, fmt.Errorf("Failed to obtain connection key %+v", keys)
	}
	defer conn.Close()
	for _, key := range keys {
		conn.Send("GET", key)
	}
	conn.Flush()
	for _, key := range keys {
		resultKey, err := redigo.String(conn.Receive())
		if err != nil && err != redigo.ErrNil {
			continue
		}
		res[key] = resultKey
	}
	return
}

// HIncrBy ...
func HIncrBy(key string, hashvalue interface{}) (int64, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int64(conn.Do("HINCRBY", redigo.Args{}.Add(key).AddFlat(hashvalue)...))
	}
	return int64(0), fmt.Errorf("Failed to obtain connection key %s", key)
}
