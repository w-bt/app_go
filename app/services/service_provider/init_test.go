package sp

import (
	"os"
	"testing"

	"bitbucket.org/w-bt/sharing/app_go/db/redis"
	"github.com/alicebob/miniredis"
)

func TestMain(m *testing.M) {
	miniRds, err := miniredis.Run()
	defer miniRds.Close()
	if err != nil {
		panic(err)
	}

	redis.InjectRedigoHost(miniRds.Addr())

	os.Exit(m.Run())
}
