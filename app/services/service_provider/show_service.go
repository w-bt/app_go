package sp

import (
	"encoding/json"
	"fmt"
	"log"

	spmod "bitbucket.org/w-bt/sharing/app_go/app/models/service_provider"
	redis "bitbucket.org/w-bt/sharing/app_go/db/redis"
)

type ShowService struct {
	guid string
}

func Show(guid string) (service *ShowService) {
	service = &ShowService{
		guid: guid,
	}

	return
}

func (s *ShowService) Call() (serviceProvider spmod.SP, err error) {
	resp, err := group.Do(fmt.Sprintf(SP_SINGLEFLIGHT_KEY, s.guid), func() (interface{}, error) {

		sp, err := s.GetFromRedis()
		if err != nil {

			sp, err = s.GetFromDB()
			if err != nil {
				log.Printf("error get from db :%+v\n", err)
				return nil, err
			}

			err = s.SetToRedis(sp)
			if err != nil {
				log.Printf("error set from db :%+v\n", err)
				return nil, err
			}
		}

		return sp, nil
	})

	if err != nil {
		log.Printf("error singleflight :%+v\n", err)
		return
	}

	serviceProvider = resp.(spmod.SP)

	return
}

func (s *ShowService) GetFromDB() (sp spmod.SP, err error) {
	sp, err = ServiceProvider().Show(s.guid)
	if err != nil {
		log.Printf("fail performing select query :%+v\n", err)
		return sp, err
	}

	return
}

func (s *ShowService) GetFromRedis() (sp spmod.SP, err error) {
	encoded, err := redis.Get(fmt.Sprintf(SP_REDIS_KEY, s.guid))
	if err != nil {
		log.Printf("error get redis key:%s :%+v\n", SP_REDIS_KEY, err)
		return
	}

	err = json.Unmarshal([]byte(encoded), &sp)
	if err != nil {
		log.Printf("fail unmarshaling service provider :%+v\n", err)
		return sp, err
	}

	return
}

func (s *ShowService) SetToRedis(sp spmod.SP) (err error) {
	rawJSON, err := json.Marshal(sp)
	if err != nil {
		log.Printf("fail marshaling service provider :%+v\n", err)
		return err
	}

	_, err = redis.Set(fmt.Sprintf(SP_REDIS_KEY, s.guid), string(rawJSON))
	if err != nil {
		log.Printf("error set redis key:%s :%+v\n", SP_REDIS_KEY, err)
		return
	}

	return
}
