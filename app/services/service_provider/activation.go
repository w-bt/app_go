package sp

import (
	"log"
	"time"

	kafkapub "bitbucket.org/w-bt/sharing/app_go/app/services/kafka_publishing"
)

type ActivationService struct{}

func Activation() (service *ActivationService) {
	service = new(ActivationService)

	return
}

func (i *ActivationService) Call() (err error) {
	spList, err := ServiceProvider().InactiveList()
	if err != nil {
		log.Printf("fail performing select query :%+v\n", err)
		return err
	}

	for _, serviceProvider := range spList {
		err = ServiceProvider().Activation(serviceProvider.ID)
		if err != nil {
			log.Printf("fail performing sp activation :%+v\n", err)
			continue
		}

		serviceProvider.IsActive = true
		serviceProvider.UpdatedAt = time.Now()
		go kafkapub.Publish(serviceProvider)
	}

	return
}
