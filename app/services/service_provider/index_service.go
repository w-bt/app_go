package sp

import (
	"log"

	spmod "bitbucket.org/w-bt/sharing/app_go/app/models/service_provider"
)

type IndexService struct {
	limit int
	skip  int
}

func Index(limit int, skip int) (service *IndexService) {
	service = &IndexService{
		limit: limit,
		skip:  skip,
	}

	return
}

func (i *IndexService) Call() (list []spmod.SP, err error) {
	resp, err := group.Do("service_provider:list", func() (interface{}, error) {

		spList, err := ServiceProvider().Index(i.limit, i.skip)
		if err != nil {
			log.Printf("fail performing select query :%+v\n", err)
			return spList, err
		}

		return spList, nil
	})

	if err != nil {
		log.Printf("error singleflight :%+v\n", err)
		return
	}

	list = resp.([]spmod.SP)

	return
}
