package serializer

import "time"

type ServiceProviderForAdmin struct {
	ID          int64     `json:"id"`
	GUID        string    `json:"guid"`
	FullName    string    `json:"full_name"`
	Email       string    `json:"email"`
	Gender      string    `json:"gender"`
	PhoneNumber string    `json:"phone_number"`
	IsActive    bool      `json:"is_active"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"-"`
	UpdatedAt   time.Time `json:"-"`
}
