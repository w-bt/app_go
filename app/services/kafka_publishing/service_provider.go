package kafkapub

import (
	"encoding/json"
	"fmt"

	spmod "bitbucket.org/w-bt/sharing/app_go/app/models/service_provider"
	conf "bitbucket.org/w-bt/sharing/app_go/config/environments"
)

func Publish(sp spmod.SP) {
	encoded, _ := json.Marshal(sp)
	Push(
		conf.ENV.Kafka.Topic,
		fmt.Sprintf("service_provider:%s", sp.GUID),
		string(encoded),
	)
}
