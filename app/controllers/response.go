package controller

import (
	"encoding/json"
	"log"
	"net/http"
)

type Response struct {
	Code          string      `json:"code"`
	Data          interface{} `json:"data,omitempty"`
	Message       string      `json:"message,omitempty"`
	Error         error       `json:"error,omitempty"`
	TimeExecution string      `json:"time_execution"`
}

func Render(w http.ResponseWriter, message string, err error, httpStatus int, data interface{}, time string) {
	w.Header().Set("Content-Type", "application/json")

	r := Response{
		Code:          http.StatusText(httpStatus),
		Data:          data,
		Message:       message,
		TimeExecution: time,
	}

	encoded, err := json.Marshal(r)
	if err != nil {
		log.Printf("marshaling response. %+v\n", err)
	}

	w.WriteHeader(httpStatus)
	w.Write(encoded)
	return
}
