package kafka

import (
	"log"

	conf "bitbucket.org/w-bt/sharing/app_go/config/environments"
	"github.com/Shopify/sarama"
)

var (
	kafkaBrokers []string
	config       *sarama.Config
)

func Init() {
	config = sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5
	config.ClientID = conf.ENV.Kafka.ClientID
	config.Producer.Return.Successes = true
	kafkaBrokers = append(kafkaBrokers, conf.ENV.Kafka.Host)
}

func Producer() (sarama.SyncProducer, error) {
	producer, err := sarama.NewSyncProducer(kafkaBrokers, config)
	if err != nil {
		log.Printf("failed to create producer %+v", err)
		return producer, err
	}

	return producer, nil
}
